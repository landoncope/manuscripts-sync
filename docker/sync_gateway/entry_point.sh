#!/bin/bash
set -e

echo "Building sync-gateway configuration…"
node node_modules/.bin/gulp compile-sync-gateway-config

# Retry initialization
#
# sync_gateway will bail if _it_ thinks indices are not available
#
# https://github.com/couchbase/sync_gateway/issues/3695
n=0
until [ $n -ge 5 ]
do
  /opt/config/wait_for_couchbase.sh \
    /sync_gateway /opt/config/docker/sync_gateway/sync-gateway-config.json \
    && break
  n=$[$n+1]
  echo "sync_gateway failed to start, retrying ($n)..."
  sleep `expr 5 \* $n`
done
