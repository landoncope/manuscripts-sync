const fs = require('fs')
const path = require('path')
const { js_beautify: beautify } = require('js-beautify')
const {
  manuscriptsFn,
  derivedDataFn,
  discussionsFn,
} = require('@manuscripts/manuscripts-json-schema')

const srcDir = path.join(__dirname, '..', 'src')
const distDir = path.join(__dirname, '..', 'dist')

// assert a directory called ./dist exists
if (!fs.existsSync(distDir)) {
  fs.mkdirSync(distDir)
}

const createSyncFunction = (inputFile, outputFile, validator) => {
  const input = fs.readFileSync(path.join(srcDir, inputFile), 'utf8')

  const searchValue = '// VALIDATOR_FUNCTION'

  if (input.indexOf(searchValue) === -1) {
    throw new Error('Nowhere to insert the validator function')
  }

  const js = input.replace(searchValue, () => validator)

  const output = beautify(js, { indent_size: 2 })

  fs.writeFileSync(path.join(distDir, outputFile), output, 'utf8')
}

createSyncFunction(
  'manuscripts-sync-fn.js',
  'manuscriptsSyncFn.js',
  manuscriptsFn
)

createSyncFunction(
  'manuscripts-sync-fn.js',
  'manuscriptsReSyncFn.js',
  'function validate(obj) { return null; }'
)

createSyncFunction(
  'derived-data-sync-fn.js',
  'derivedDataSyncFn.js',
  derivedDataFn
)

createSyncFunction(
  'discussions-sync-fn.js',
  'discussionsSyncFn.js',
  discussionsFn
)
