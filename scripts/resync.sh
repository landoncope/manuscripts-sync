#!/usr/bin/env bash

# bind the admin ports of instances to 7001...700(N)
# kubectl port-forward -n development development-syncgw-5d5d55b6bd-hj456 7001:4985
# kubectl port-forward -n development development-syncgw-5d5d55b6bd-z4ghs 7002:4985

set -e

# set to 1 if running a resync with a replacement sync function.
SHOULD_REPLACE_SYNC_CONFIG=0
REPLACEMENT_CONFIG_JSON="replacement-config.json"

if [ $SHOULD_REPLACE_SYNC_CONFIG -eq 1 ]
then
  if [ -f "$REPLACEMENT_CONFIG_JSON" ]
  then
    echo "$REPLACEMENT_CONFIG_JSON found."
  else
    echo "$REPLACEMENT_CONFIG_JSON not found."
    echo "Build a replacement bucket config and save it as $REPLACEMENT_CONFIG_JSON"
    exit 1
  fi
fi

ports=(7001 7002)

# use arbitrary node for running resync operation
resyncer="${ports[0]}"

if [ $SHOULD_REPLACE_SYNC_CONFIG -eq 1 ]
then
  printf "Backing up existing sync fn...\n"
  curl --fail "http://localhost:$resyncer/projects/_config" > backup-config.json
fi

printf "Bringing down sync_gateway instances...\n"

# Bring down sync_gateway instances
for i in "${!ports[@]}"
do
  port="${ports[$i]}"
  curl --fail -XPOST "http://localhost:$port/projects/_offline"
  curl --silent "http://localhost:$port/projects/" | grep -q '"state":"Offline"' &&
    printf "Offline: $port\n" || printf "Failed to bring offline: $port\n"
done

printf "\n"

if [ $SHOULD_REPLACE_SYNC_CONFIG -eq 1 ]
then
  printf "Updating $resyncer with validator-less sync fn...\n"
  curl --fail -X PUT -T "./config.json" "http://localhost:$resyncer/projects/_config"
fi

curl --fail -s -XPOST "http://localhost:$resyncer/projects/_resync"

printf "\nResync complete\n\n"

if [ $SHOULD_REPLACE_SYNC_CONFIG -eq 1 ]
then
  printf "Reinstating original sync fn on $resyncer...\n"
  curl --fail -X PUT -T "./backup-config.json" "http://localhost:$resyncer/projects/_config"
fi

printf "Bringing up sync_gateway instances...\n"
# Bring up sync_gateway instances
for i in "${!ports[@]}"
do
  port="${ports[$i]}"
  curl --fail -XPOST "http://localhost:$port/projects/_online"
  curl --silent "http://localhost:$port/projects/" | grep -q '"state":"Online"' &&
    printf "Online: $port\n" || printf "Failed to bring online: $port\n"
done

printf "\n\nComplete"
