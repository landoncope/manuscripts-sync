function syncFn(doc, oldDoc) {
  // VALIDATOR_FUNCTION

  requireAdmin();

  if (!doc._deleted) {
    if (oldDoc && oldDoc.objectType !== doc.objectType) {
      // prettier-ignore
      throw({ forbidden: 'objectType cannot be mutated' });
    }

    var errorMessage = validate(doc);

    if (errorMessage) {
      // prettier-ignore
      throw({ forbidden: errorMessage });
    }
  }

  function getField(key) {
    return doc._deleted ? oldDoc[key] : doc[key];
  }

  function objectTypeMatches(arg) {
    return getField('objectType') === arg;
  }

  if (!doc._deleted) {
    var userID = getField('userID');
    if (objectTypeMatches('MPUserCollaborator')) {
      var collaboratorsChannel = userID + '-collaborators';
      channel(collaboratorsChannel);
      access(userID, collaboratorsChannel);
    } else if (objectTypeMatches('MPProjectMemento')) {
      var projectMementosChannel = userID + '-project-mementos';
      channel(projectMementosChannel);
      access(userID, projectMementosChannel);
    } else if (
      objectTypeMatches('MPLibrarySummary') ||
      objectTypeMatches('MPLibraryCollectionSummary') ||
      objectTypeMatches('MPProjectSummary')
    ) {
      var containerID = getField('containerID');
      var containerSummaryChannel = containerID + '-summaries';
      channel(containerSummaryChannel);

      var ownersIDs = doc.ownerProfiles.map(function(profile) {
        return profile.userID;
      });
      var writersIDs = doc.writerProfiles.map(function(profile) {
        return profile.userID;
      });
      var viewersIDs = doc.viewerProfiles.map(function(profile) {
        return profile.userID;
      });

      access(ownersIDs, containerSummaryChannel);
      access(writersIDs, containerSummaryChannel);
      access(viewersIDs, containerSummaryChannel);
    }
  }
}

// this is how typescript generates exports.
// they work for JS and TS (i.e. with .d.ts file).
Object.defineProperty(exports, '__esModule', { value: true });
exports.syncFn = syncFn;
