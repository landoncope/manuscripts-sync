function syncFn(doc, oldDoc) {
  // VALIDATOR_FUNCTION

  // make sure the user is not editing a deleted object
  if (oldDoc && oldDoc.removed && doc.removed) {
    throw { forbidden: 'deleted document cannot be mutated' };
  }

  // make sure objectType exist
  if (doc && !doc.objectType) {
    // prettier-ignore
    throw({ forbidden: 'objectType is missing' });
  }

  // make sure the objectType is not edited
  if (oldDoc && oldDoc.objectType !== doc.objectType) {
    // prettier-ignore
    throw({ forbidden: 'objectType cannot be mutated' });
  }

  // make sure _id is not missing
  if (!doc._id) {
    throw { forbidden: '_id is missing!' };
  }

  // make sure Id Prefix match objectType
  if (doc._id.substr(0, doc._id.indexOf(':')) !== doc.objectType) {
    throw { forbidden: '_id must have objectType as prefix' };
  }

  if (oldDoc && oldDoc.dateInserted !== doc.dateInserted) {
    throw { forbidden: 'dateInserted cannot be mutated' };
  }

  if (doc && !doc.dateInserted) {
    throw { forbidden: 'dateInserted is missing' };
  }

  var errorMessage = validate(doc);

  if (errorMessage) {
    throw { forbidden: errorMessage };
  }

  if (doc.objectType === 'MPDiscussionItem') {
    //  make sure the user who created the object exist
    if (!doc.users || !doc.users[0] || !doc.users[0].userId) {
      throw { forbidden: 'users are missing' };
    }
    var owner = doc.users[0];
    // make sure publication access exist(doi, url ...)
    if (
      !doc.publicationAccess ||
      (doc.publicationAccess.access !== 0 && !doc.publicationAccess.access) ||
      (doc.publicationAccess.type !== 0 && !doc.publicationAccess.type) ||
      !doc.publicationAccess.value
    ) {
      // prettier-ignore
      throw({ forbidden: 'publicationAccess is missing' });
    }
    // make sure publication access is not changed
    if (
      oldDoc &&
      (oldDoc.publicationAccess.access !== doc.publicationAccess.access ||
        oldDoc.publicationAccess.value !== doc.publicationAccess.value ||
        oldDoc.publicationAccess.type !== doc.publicationAccess.type)
    ) {
      // prettier-ignore
      throw({ forbidden: 'publicationAccess cannot be mutated' });
    }
    //make sure the owner of the object is not edited
    if (
      oldDoc &&
      (oldDoc.users[0].userId !== owner.userId ||
        oldDoc.users[0].role !== owner.role ||
        oldDoc.users[0].sgUserId !== owner.sgUserId)
    ) {
      // prettier-ignore
      throw({ forbidden: 'cannot change owner' });
    }
    // this will indicate that the type of object is wrong and has no value
    if (!doc.section && !doc.text) {
      // prettier-ignore
      throw({ forbidden: "does not represent a note or annotation"});
    }
    // section should not be edited
    if (oldDoc && oldDoc.section !== doc.section) {
      throw { forbidden: 'cannot change section!' };
    }

    if (owner.sgUserId) {
      var sgUserId = owner.sgUserId.replace('|', '_');
      requireUser(sgUserId);
      channel(doc._id);
      access(sgUserId, doc._id);
    }
  } else {
    throw { forbidden: 'unsupported Type' };
  }
}

// this is how typescript generates exports.
// they work for JS and TS (i.e. with .d.ts file).
Object.defineProperty(exports, '__esModule', { value: true });
exports.syncFn = syncFn;
