module.exports = {
  env: {
    commonjs: true,
    es6: false,
    node: false,
  },
  extends: ['plugin:prettier/recommended'],
  parserOptions: {
    ecmaVersion: 5,
  },
  rules: {
    'no-throw-literal': 0,
    'no-var': 0,
  },
  globals: {
    access: 'readonly',
    channel: 'readonly',
    equal: 'readonly',
    exports: 'readonly',
    requireAccess: 'readonly',
    requireAdmin: 'readonly',
    requireUser: 'readonly',
    validate: 'readonly',
  },
};
