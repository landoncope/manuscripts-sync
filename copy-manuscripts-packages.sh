#!/bin/bash
set -e

if [ "$COPY_LOCAL_PACKAGES" = "true" ]; then
  echo "Copying in @manuscripts/manuscripts-sync and @manuscripts/manuscripts-json-schema."
  mkdir -p node_modules/@manuscripts
  cp -r ./local/@manuscripts/manuscripts-sync node_modules/@manuscripts/manuscripts-sync
  cp -r ./local/@manuscripts/manuscripts-json-schema/dist node_modules/@manuscripts/manuscripts-json-schema
else
  echo "Using @manuscripts/manuscripts-sync from NPM (version ${MANUSCRIPTS_SYNC_VERSION})"
  npm install --no-save --no-package-lock --no-optional @manuscripts/manuscripts-sync${MANUSCRIPTS_SYNC_VERSION}
fi
