module.exports = {
  env: {
    commonjs: true,
    es6: true,
    node: true,
  },
  extends: ['plugin:prettier/recommended'],
  parserOptions: {
    ecmaVersion: 2018,
  },
  rules: {
    'no-undef': 2,
    'no-var': 2,
    'quote-props': [2, 'as-needed'],
  },
}
