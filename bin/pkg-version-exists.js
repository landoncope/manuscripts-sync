#!/usr/bin/env node

const cp = require('child_process')
const pkg = require(`${__dirname}/../package.json`)

const info = cp.execSync(`npm view ${pkg.name} versions --json --verbose`, {
  encoding: 'utf-8',
})

const published = JSON.parse(info).includes(pkg.version) ? 'true' : 'false'

console.log(published)
