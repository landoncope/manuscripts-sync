const { createReporter } = require('istanbul-api')
const { createCoverageMap } = require('istanbul-lib-coverage')

function hookStdout(callback) {
  const oldWrite = process.stdout.write
  process.stdout.write = callback
  return () => {
    process.stdout.write = oldWrite
  }
}

function getCoverageMin(coverageTable, scriptName) {
  const re = new RegExp(`${scriptName.replace('.', '\\.')}.+`)
  const [syncFnLine] = coverageTable.match(re)
  const [, statements, branch, funcs, lines] = syncFnLine.split('|')
  const f = result => parseFloat(result.match(/\s+([\d.]+)/)[1])
  return Math.min(f(statements), f(branch), f(funcs), f(lines))
}

function coverage(__coverage__, scriptNames) {
  const map = createCoverageMap(__coverage__)
  const reporter = createReporter()
  reporter.add('text')
  let coverageTable = ''
  const unhookStdout = hookStdout(str => {
    coverageTable += str
    return coverageTable
  })
  reporter.write(map)
  unhookStdout()
  console.log(coverageTable)
  for (const scriptName of scriptNames) {
    const min = getCoverageMin(coverageTable, scriptName)
    console.log(
      `${scriptName} coverage:\u001b[3${
        min === 100 ? 2 : 1
      };1m ${min} \u001b[0m`
    )
    // if (min !== 100) {
    //   console.log('\n\u001b[31;1mFailed to meet 100% coverage\u001b[0m');
    //   process.exit(1);
    // }
  }
}

module.exports = {
  coverage,
}
