const test = require('tape')
const vm = require('vm')
const sinon = require('sinon')
const { createInstrumenter } = require('istanbul-lib-instrument')

const { derivedDataSyncFn } = require('../dist')

const SCRIPT_NAME = 'derivedDataSyncFn.js'

module.exports = { SCRIPT_NAME }

// instrument syncFn (add a counter above every line)
const instrumentedCode = createInstrumenter({}).instrumentSync(
  derivedDataSyncFn.toString(),
  SCRIPT_NAME
)

// share coverage between vm executions
global.__coverage__ = {}

function execute(obj, oldObj, sandboxConfigure) {
  const channel = sinon.spy()
  const access = sinon.spy()
  const requireAccess = sinon.spy()
  const requireUser = sinon.spy()
  const requireAdmin = sinon.spy()
  const sandbox = {
    access,
    channel,
    obj,
    oldObj,
    requireAccess,
    requireAdmin,
    requireUser,
    Object,
    Array,
    __coverage__,
  }
  if (sandboxConfigure) {
    sandboxConfigure(sandbox)
  }
  // Create JS context where our scripts will be executed.
  const context = vm.createContext(sandbox)
  // Execute main script with validate function
  const mainScript = new vm.Script(instrumentedCode)
  mainScript.runInContext(context)
  // Run validate against obj
  const testScript = new vm.Script('syncFn(obj, oldObj)')
  try {
    testScript.runInContext(context)
    return sandbox
  } catch (e) {
    // console.error(e)
    throw e
  }
}

test('admin required', t => {
  t.plan(1)

  const validObject = {
    objectType: 'MPUserCollaborator',
    _rev: '1-cf3758c6a77c031dcd8f617087c7493d',
    _id: 'MPUserCollaborator:15326C7B-836D-4D6C-81EB-7E6CA6153E9A',
    userID: 'User_foobar@manuscriptsapp.com',
    collaboratorID: 'MPUserProfile:foo-bar-baz',
    projects: {
      owner: [],
      writer: [],
      viewer: [],
    },
    collaboratorProfile: {
      _id: 'MPUserProfile:foo-bar-baz',
      objectType: 'MPUserProfile',
      createdAt: 1515417692.477127,
      updatedAt: 1515494608.363229,
      bibliographicName: {
        _id: 'MPBibliographicName:foo-bar-baz',
        objectType: 'MPBibliographicName',
        createdAt: 1515417692.477127,
        updatedAt: 1515494608.363229,
      },
      userID: 'User_foobar@manuscriptsapp.com',
    },
    createdAt: 1515417692.477127,
    updatedAt: 1515494608.363229,
  }

  t.ok(
    execute(Object.assign({}, validObject), null).requireAdmin.called,
    'requireAdmin called when trying to update userID in MPUserProject'
  )
})

test('user collaborator', t => {
  t.plan(7)

  const validObject = {
    objectType: 'MPUserCollaborator',
    _rev: '1-cf3758c6a77c031dcd8f617087c7493d',
    _id: 'MPUserCollaborator:15326C7B-836D-4D6C-81EB-7E6CA6153E9A',
    userID: 'User_foobar@manuscriptsapp.com',
    collaboratorID: 'MPUserProfile:foo-bar-baz',
    projects: {
      owner: [],
      writer: [],
      viewer: [],
    },
    collaboratorProfile: {
      _id: 'MPUserProfile:foo-bar-baz',
      objectType: 'MPUserProfile',
      createdAt: 1515417692.477127,
      updatedAt: 1515494608.363229,
      bibliographicName: {
        _id: 'MPBibliographicName:foo-bar-baz',
        objectType: 'MPBibliographicName',
        createdAt: 1515417692.477127,
        updatedAt: 1515494608.363229,
      },
      userID: 'User_foobar@manuscriptsapp.com',
    },
    createdAt: 1515417692.477127,
    updatedAt: 1515494608.363229,
  }

  t.ok(execute(Object.assign({}, validObject), null))

  t.throws(() => {
    execute(
      Object.assign({}, validObject),
      Object.assign({}, validObject, { objectType: 'MPFoo' })
    )
  }, 'cannot mutate objectType')

  t.ok(
    execute(Object.assign({}, validObject), null).channel.calledWith(
      validObject.userID + '-collaborators'
    ),
    'channel function called with {userID}-collaborators'
  )

  t.ok(
    execute(Object.assign({}, validObject), null).access.calledWith(
      validObject.userID,
      validObject.userID + '-collaborators'
    ),
    'access function called with {userID} and {userID}-collaborators'
  )

  t.ok(
    execute(
      {
        _deleted: true,
        _id: validObject._id,
      },
      Object.assign({}, validObject)
    )
  )

  t.ok(execute(Object.assign({}, validObject.collaboratorProfile)))

  t.throws(() => {
    execute(Object.assign({}, validObject, { foo: 'foo' }))
  })
})

test('project memento', t => {
  t.plan(7)

  const validObject = {
    objectType: 'MPProjectMemento',
    _rev: '1-cf3758c6a77c031dcd8f617087c7493d',
    _id: 'MPProjectMemento:15326C7B-836D-4D6C-81EB-7E6CA6153E9A',
    userID: 'User_foobar@manuscriptsapp.com',
    projectID: 'MPProject:foo-bar-baz',
    project: {
      _id: 'MPProject:foo-bar-baz',
      objectType: 'MPProject',
      createdAt: 1515417692.477127,
      updatedAt: 1515494608.363229,
      writers: [],
      viewers: [],
      owners: ['User_foobar@manuscriptsapp.com'],
    },
    createdAt: 1515417692.477127,
    updatedAt: 1515494608.363229,
  }

  t.ok(execute(Object.assign({}, validObject), null))

  t.throws(() => {
    execute(
      Object.assign({}, validObject),
      Object.assign({}, validObject, { objectType: 'MPFoo' })
    )
  }, 'cannot mutate objectType')

  t.ok(
    execute(Object.assign({}, validObject), null).channel.calledWith(
      validObject.userID + '-project-mementos'
    ),
    'channel function called with {userID}-project-mementos'
  )

  t.ok(
    execute(Object.assign({}, validObject), null).access.calledWith(
      validObject.userID,
      validObject.userID + '-project-mementos'
    ),
    'access function called with {userID} and {userID}-project-mementos'
  )

  t.ok(
    execute(
      {
        _deleted: true,
        _id: validObject._id,
      },
      Object.assign({}, validObject)
    )
  )

  t.ok(execute(Object.assign({}, validObject.project)))

  t.throws(() => {
    execute(Object.assign({}, validObject, { foo: 'foo' }))
  })
})

test('project summary', t => {
  t.plan(5)

  const validObject = {
    _id: 'MPProjectSummary:MPProject:valid-project-id-summary',
    containerID: 'MPProject:valid-project-id-summary',
    createdAt: 1568634366.363,
    lastModifiedDocumentID: 'MPProject:valid-project-id-summary',
    objectType: 'MPProjectSummary',
    ownerProfiles: [
      {
        _id: 'MPUserProfile:valid-user-profile',
        bibliographicName: {
          _id: 'MPBibliographicName:valid-bibliographic-name',
          given: 'Kevin',
          objectType: 'MPBibliographicName',
        },
        createdAt: 1568639779.784,
        objectType: 'MPUserProfile',
        updatedAt: 1568639779.784,
        userID: 'User_valid-user-1@manuscriptsapp.com',
      },
    ],
    updatedAt: 1568639786.45,
    viewerProfiles: [
      {
        _id: 'MPUserProfile:valid-user-profile-2',
        bibliographicName: {
          _id: 'MPBibliographicName:valid-bibliographic-name',
          given: 'Prince',
          objectType: 'MPBibliographicName',
        },
        createdAt: 1568639779.801,
        objectType: 'MPUserProfile',
        updatedAt: 1568639779.801,
        userID: 'User_valid-user-2@manuscriptsapp.com',
      },
    ],
    writerProfiles: [],
  }

  t.ok(execute(Object.assign({}, validObject), null))

  t.throws(() => {
    execute(
      Object.assign({}, validObject),
      Object.assign({}, validObject, { objectType: 'MPFoo' })
    )
  }, 'cannot mutate objectType')

  t.ok(
    execute(Object.assign({}, validObject), null).channel.calledWith(
      validObject.containerID + '-summaries'
    ),
    'channel function called with {containerID}-summaries'
  )

  t.ok(
    execute(Object.assign({}, validObject), null).access.calledWith(
      ['User_valid-user-1@manuscriptsapp.com'],
      validObject.containerID + '-summaries'
    ),
    'access function called with {userID} and {containerID}-summaries'
  )

  t.ok(
    execute(Object.assign({}, validObject), null).access.calledWith(
      ['User_valid-user-2@manuscriptsapp.com'],
      validObject.containerID + '-summaries'
    ),
    'access function called with {userID} and {containerID}-summaries'
  )
})
