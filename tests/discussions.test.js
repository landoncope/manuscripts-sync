const test = require('tape')
const vm = require('vm')
const sinon = require('sinon')
const { createInstrumenter } = require('istanbul-lib-instrument')

const { discussionsSyncFn } = require('../dist')

const SCRIPT_NAME = 'discussionsSyncFn.js'

module.exports = { SCRIPT_NAME }

// instrument syncFn (add a counter above every line)
const instrumentedCode = createInstrumenter({}).instrumentSync(
  discussionsSyncFn.toString(),
  SCRIPT_NAME
)

// share coverage between vm executions
global.__coverage__ = {}

function execute(obj, oldObj = null, sandboxConfigure) {
  const channel = sinon.spy()
  const access = sinon.spy()
  const requireAccess = sinon.spy()
  const requireUser = sinon.spy()
  const requireAdmin = sinon.spy()
  const sandbox = {
    access,
    channel,
    obj,
    oldObj,
    requireAccess,
    requireAdmin,
    requireUser,
    Object,
    Array,
    __coverage__,
  }
  if (sandboxConfigure) {
    sandboxConfigure(sandbox)
  }
  // Create JS context where our scripts will be executed.
  const context = vm.createContext(sandbox)
  // Execute main script with validate function
  const mainScript = new vm.Script(instrumentedCode)
  mainScript.runInContext(context)
  // Run validate against obj
  const testScript = new vm.Script('syncFn(obj, oldObj)')
  try {
    testScript.runInContext(context)
    return sandbox
  } catch (e) {
    console.error(e)
    throw e
  }
}

test('random objects without MP-prefix', t => {
  t.plan(4)

  t.throws(() => {
    execute({ objectType: 'Foo', _id: 'Foo:foo' }, null)
  })

  t.throws(() => {
    execute({ objectType: 'Foo' }, null)
  })

  t.throws(() => {
    execute({ _id: 'Foo' }, null)
  })

  t.throws(() => {
    execute({ _id: 'Foo', objectType: 'MPDiscussionItem' }, null)
  })
})

test('supported types', t => {
  t.plan(1)
  t.throws(() => {
    execute(
      {
        _id: 'MPDiscussionUnSupported:Foo',
        objectType: 'MPDiscussionUnSupported',
      },
      null
    )
  })
})

test('object delete', t => {
  t.plan(4)

  const validAnnotation = {
    _id: 'MPDiscussionItem:AC5481D8-ECE7-1962-B74D-53F97103CA15',
    contextRanges: [
      {
        contentType: 'HTML',
        context: 'http://article.example.com:9080/moby_dick.html',
        offset: 8747,
        offset_h: 188,
        offset_t: 24586,
        ranges: [
          {
            end: '/div[2]/p[44]',
            endOffset: 36,
            start: '/div[2]/p[43]',
            startOffset: 22,
          },
        ],
      },
    ],
    objectType: 'MPDiscussionItem',
    publicationAccess: {
      access: 1,
      type: 2,
      value: 'article.example.com:9080/moby_dick.html',
    },
    quoteHTML:
      'whale stranded on the shores of Europe.” —<I>Edmund<br/>Burke</I>. (<I>somewhere</I>.)<br/>“A tenth branch of the king',
    section:
      'whale stranded on the shores of Europe.” —Edmund Burke. (somewhere.) “A tenth branch of the king',
    sendResolutionMessageToSubmitter: false,
    siteUrl: 'http://article.example.com:9080/moby_dick.html',
    subjects: [],
    text: 'New',
    dateInserted: 1594036894777,
    users: [
      {
        role: 0,
        sgUserId: 'User|hmshreim+0001@redlink.com',
        userId: '5ee947b7155c474c5f506aa8',
      },
    ],
  }

  const validNote = {
    _id: 'MPDiscussionItem:AC5481D8-ECE7-1962-B74D-53F97103CA16',
    dateInserted: 1593942480670,
    objectType: 'MPDiscussionItem',
    publicationAccess: {
      access: 1,
      type: 2,
      value: 'article.example.com:9080/moby_dick.html',
    },
    section: null,
    sendResolutionMessageToSubmitter: false,
    text: '8',
    users: [
      {
        role: 0,
        sgUserId: 'User|hmshreim+0001@redlink.com',
        userId: '5ee947b7155c474c5f506aa8',
      },
    ],
  }

  t.ok(
    execute(
      Object.assign({}, validAnnotation, { removed: true }),
      Object.assign({}, validAnnotation)
    )
  )

  t.ok(
    execute(
      Object.assign({}, validAnnotation),
      Object.assign({}, validAnnotation, { removed: true })
    )
  )

  t.ok(
    execute(
      Object.assign({}, validNote, { removed: true }),
      Object.assign({}, validNote)
    )
  )

  t.ok(
    execute(
      Object.assign({}, validNote),
      Object.assign({}, validNote, { removed: true })
    )
  )
})

test('user', t => {
  t.plan(6)
  const baseNote = {
    _id: 'MPDiscussionItem:AC5481D8-ECE7-1962-B74D-53F97103CA16',
    dateInserted: 1593942480670,
    objectType: 'MPDiscussionItem',
    publicationAccess: {
      access: 1,
      type: 2,
      value: 'article.example.com:9080/moby_dick.html',
    },
    section: null,
    sendResolutionMessageToSubmitter: false,
    text: '8',
  }

  t.throws(() => {
    execute(Object.assign({}, baseNote, { users: [] }))
  }, '.users: should NOT have fewer than 1 items')

  t.throws(() => {
    execute(Object.assign({}, baseNote))
  }, "should have required property 'users'")

  t.throws(() => {
    execute(
      Object.assign({}, baseNote, {
        users: [
          {
            role: 0,
            sgUserId: 'User|hmshreim+0001@redlink.com',
            userId: '5ee947b7155c474c5f506aa8',
          },
        ],
      }),
      Object.assign({}, baseNote, {
        users: [
          {
            role: 1,
            sgUserId: 'User|hmshreim+0001@redlink.com',
            userId: '5ee947b7155c474c5f506aa8',
          },
        ],
      })
    )
  }, 'cannot change owner')

  t.throws(() => {
    execute(
      Object.assign({}, baseNote, {
        users: [
          {
            role: 0,
            sgUserId: 'User|hmshreim+0001@redlink.com',
            userId: '5ee947b7155c474c5f506aa8',
          },
        ],
      }),
      Object.assign({}, baseNote, {
        users: [
          {
            role: 0,
            sgUserId: 'User|hmshreim+0002@redlink.com',
            userId: '5ee947b7155c474c5f506aa8',
          },
        ],
      })
    )
  }, 'cannot change owner')

  t.throws(() => {
    execute(
      Object.assign({}, baseNote, {
        users: [
          {
            role: 0,
            sgUserId: 'User|hmshreim+0001@redlink.com',
            userId: '5ee947b7155c474c5f506aa8',
          },
        ],
      }),
      Object.assign({}, baseNote, {
        users: [
          {
            role: 0,
            sgUserId: 'User|hmshreim+0001@redlink.com',
            userId: '5ee947b7155c474c5f506aa9',
          },
        ],
      })
    )
  }, 'cannot change owner')

  t.ok(
    execute(
      Object.assign({}, baseNote, {
        users: [
          {
            role: 0,
            sgUserId: 'User|hmshreim+0001@redlink.com',
            userId: '5ee947b7155c474c5f506aa9',
          },
        ],
      }),
      Object.assign({}, baseNote, {
        users: [
          {
            role: 0,
            sgUserId: 'User|hmshreim+0001@redlink.com',
            userId: '5ee947b7155c474c5f506aa9',
          },
        ],
      })
    )
  )
})

test('publicationAccess', t => {
  t.plan(9)
  const baseNote = {
    _id: 'MPDiscussionItem:AC5481D8-ECE7-1962-B74D-53F97103CA16',
    dateInserted: 1593942480670,
    objectType: 'MPDiscussionItem',
    section: null,
    sendResolutionMessageToSubmitter: false,
    text: '8',
    users: [
      {
        role: 0,
        sgUserId: 'User|hmshreim+0001@redlink.com',
        userId: '5ee947b7155c474c5f506aa8',
      },
    ],
  }

  t.throws(() => {
    execute(
      Object.assign({}, baseNote),
      Object.assign({}, baseNote, {
        publicationAccess: {
          access: 1,
          type: 2,
          value: 'article.example.com:9080/moby_dick.html',
        },
      })
    )
  })

  t.throws(() => {
    execute(
      Object.assign({}, baseNote, {
        publicationAccess: {
          access: 1,
          type: 2,
          value: 'article.example.com:9080/moby_dick.html',
        },
      }),
      Object.assign({}, baseNote, {
        publicationAccess: {
          type: 2,
          value: 'article.example.com:9080/moby_dick.html',
        },
      })
    )
  })

  t.throws(() => {
    execute(
      Object.assign({}, baseNote, {
        publicationAccess: {
          access: 1,
          type: 2,
          value: 'article.example.com:9080/moby_dick.html',
        },
      }),
      Object.assign({}, baseNote, {
        publicationAccess: {
          access: 1,
          value: 'article.example.com:9080/moby_dick.html',
        },
      })
    )
  })

  t.throws(() => {
    execute(
      Object.assign({}, baseNote, {
        publicationAccess: {
          access: 1,
          type: 2,
          value: 'article.example.com:9080/moby_dick.html',
        },
      }),
      Object.assign({}, baseNote, {
        publicationAccess: {
          access: 1,
          type: 2,
        },
      })
    )
  })

  t.throws(() => {
    execute(
      Object.assign({}, baseNote, {
        publicationAccess: {
          access: 1,
          type: 2,
          value: 'article.example.com:9080/moby_dick.html',
        },
      }),
      Object.assign({}, baseNote, {
        publicationAccess: {
          access: 2,
          type: 2,
          value: 'article.example.com:9080/moby_dick.html',
        },
      })
    )
  })

  t.throws(() => {
    execute(
      Object.assign({}, baseNote, {
        publicationAccess: {
          access: 1,
          type: 2,
          value: 'article.example.com:9080/moby_dick.html',
        },
      }),
      Object.assign({}, baseNote, {
        publicationAccess: {
          access: 1,
          type: 3,
          value: 'article.example.com:9080/moby_dick.html',
        },
      })
    )
  })

  t.throws(() => {
    execute(
      Object.assign({}, baseNote, {
        publicationAccess: {
          access: 1,
          type: 2,
          value: 'article.example.com:9080/moby_dick.html',
        },
      }),
      Object.assign({}, baseNote, {
        publicationAccess: {
          access: 1,
          type: 2,
          value: 'article.example.com:9080/moby_dick2.html',
        },
      })
    )
  })

  t.ok(
    execute(
      Object.assign({}, baseNote, {
        publicationAccess: {
          access: 1,
          type: 2,
          value: 'article.example.com:9080/moby_dick.html',
        },
      }),
      Object.assign({}, baseNote, {
        publicationAccess: {
          access: 1,
          type: 2,
          value: 'article.example.com:9080/moby_dick.html',
        },
      })
    )
  )

  t.ok(
    execute(
      Object.assign({}, baseNote, {
        publicationAccess: {
          access: 0,
          type: 0,
          value: 'article.example.com:9080/moby_dick.html',
        },
      }),
      Object.assign({}, baseNote, {
        publicationAccess: {
          access: 0,
          type: 0,
          value: 'article.example.com:9080/moby_dick.html',
        },
      })
    )
  )
})

test('note or annotations', t => {
  t.plan(4)

  const baseNote = {
    _id: 'MPDiscussionItem:AC5481D8-ECE7-1962-B74D-53F97103CA16',
    dateInserted: 1593942480670,
    publicationAccess: {
      access: 1,
      type: 2,
      value: 'article.example.com:9080/moby_dick.html',
    },
    objectType: 'MPDiscussionItem',
    sendResolutionMessageToSubmitter: false,
    users: [
      {
        role: 0,
        sgUserId: 'User|hmshreim+0001@redlink.com',
        userId: '5ee947b7155c474c5f506aa8',
      },
    ],
    attachments: {
      images: [
        {
          type: 'png',
          link: 'https://www.fnordware.com/superpng/pnggrad8rgb.png',
          name: 'Random PNG',
          size: 976,
        },
        {
          type: 'png',
          link: 'https://www.fnordware.com/superpng/pnggrad8rgb.png',
          name: 'Random PNG 2',
          size: 976,
        },
      ],
      files: [],
      videos: [],
      others: [],
    },
  }

  t.throws(() => {
    execute(
      Object.assign({}, baseNote),
      Object.assign({}, baseNote, { text: 'test' })
    )
  })

  t.ok(
    execute(
      Object.assign({}, baseNote, { text: 'test1' }),
      Object.assign({}, baseNote, { text: 'test' })
    )
  )

  t.throws(() => {
    execute(
      Object.assign({}, baseNote, { section: 'test1' }),
      Object.assign({}, baseNote, { section: 'test' })
    )
  })

  t.ok(
    execute(
      Object.assign({}, baseNote, { section: 'test' }),
      Object.assign({}, baseNote, { section: 'test' })
    )
  )
})

test('test dateInserted', t => {
  t.plan(3)

  const baseNote = {
    _id: 'MPDiscussionItem:AC5481D8-ECE7-1962-B74D-53F97103CA16',
    objectType: 'MPDiscussionItem',
    publicationAccess: {
      access: 1,
      type: 2,
      value: 'article.example.com:9080/moby_dick.html',
    },
    section: null,
    sendResolutionMessageToSubmitter: false,
    text: '8',
    users: [
      {
        role: 0,
        sgUserId: 'User|hmshreim+0001@redlink.com',
        userId: '5ee947b7155c474c5f506aa8',
      },
    ],
  }

  t.throws(() => {
    execute(
      Object.assign({}, baseNote, { dateInserted: 1593942480670 }),
      Object.assign({}, baseNote, { dateInserted: 1593942480671 })
    )
  })

  t.throws(() => {
    execute(Object.assign({}, baseNote), Object.assign(baseNote))
  })

  t.ok(
    execute(
      Object.assign({}, baseNote, { dateInserted: 1593942480670 }),
      Object.assign({}, baseNote, { dateInserted: 1593942480670 })
    )
  )
})
